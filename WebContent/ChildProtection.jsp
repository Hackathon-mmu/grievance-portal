<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Child Protection</title>
          <meta charset="utf-8">
          <meta name="viewport" content="width=device-width, initial-scale=1">
          <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
          <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
          <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
          <style>
            /* Remove the navbar's default margin-bottom and rounded borders */ 
            .navbar {
              margin-bottom: 0;
              border-radius: 0;
              background-color: #3e4444;
            }

            input{
                border: 1px solid #ccc;
                border-radius: 4px;
                box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
            }
            
            /* Set height of the grid so .sidenav can be 100% (adjust as needed) */
            .row.content {height: 450px}
            
            /* Set gray background color and 100% height */
            .sidenav {
              padding-top: 20px;
              background-color: #f1f1f1;
              min-height: 655px;
              height: 100%;
              padding-bottom: 20px;
            }
            
            /* Set black background color, white text and some padding */
            footer {
              background-color: #555;
              color: white;
              padding: 15px;
            }
            
            /* On small screens, set height to 'auto' for sidenav and grid */
            @media screen and (max-width: 767px) {
              .sidenav {
                padding: 15px;
              }
              .row.content {height:auto;} 
            }
            .well{
                margin-bottom: 5px;
                padding: 10px;
            }
            hr {
                width :200px;
                border-top: 1px solid #f8f8f8;
                border-bottom: 1px solid rgba(0,0,0,0.2);

            }
          </style>
</head>
<body>
<%
	String userId = (String) session.getAttribute("userId");
%>
<%@ include file="WelcomeHeader.html" %>
<!-- left sidebar -->  
        <div class="container-fluid text-center">    
          <div class="row content">
            <div class="col-sm-3 well">
              <div class="well">
                <p><a href="#">My Profile<%= userId %></a></p>
                <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/d/d6/Bald_Eagle_Head_2_%286021915997%29.jpg/400px-Bald_Eagle_Head_2_%286021915997%29.jpg" class="img-circle" height="65" width="65" alt="Avatar">
              </div>

               <div class="alert alert-success fade in">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                <p><strong>Ey!</strong></p>
                search nearby to know, how you can help your society!.
              </div>

              <div class="well">
                <p><a href="#">More</a></p>
                 <p>
                  <input style="margin-top: 5px; width: 75%; " type="submit" value="About US">
                    <br>
                  <input style="margin-top: 5px; width: 75%;" type="submit" value="Doanate Here">
                    <br>
                  <input style="margin-top: 5px; width: 75%;" type="submit" value="Events">
                    <br>
                  <input style="margin-top: 5px; width: 75%;" type="submit" value="Contact Us">
                    <br>
                  <input style="margin-top: 5px; width: 75%;" type="submit" value="NGOs Involve">
                    <br>
                </p>
              </div>
              <div class="thumbnail">
                    <p>Upcoming Events:</p>
                    <img src="image/universumm.png" alt="Paris" width="400" height="300">
                    <p><strong>UniversuMM</strong> Star Night</p>
                    <p>Monday. 12 September 2017</p>
                    <button href="http://universumm.in/" class="btn btn-info">Learn More</button>
                </div>    
            </div>
        <!-- main central content -->
            <div class="col-sm-6 text-left container"> 
              <h1 style="text-align: center;">Child protection</h1>
              <hr>
              <div id="myCarousel" class="carousel slide" data-ride="carousel">
                  <!-- Indicators -->
                  <ol class="carousel-indicators">
                    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                    <li data-target="#myCarousel" data-slide-to="1"></li>
                    <li data-target="#myCarousel" data-slide-to="2"></li>
                    <li data-target="#myCarousel" data-slide-to="3"></li>
                    <li data-target="#myCarousel" data-slide-to="4"></li>
                  </ol>

                  <!-- Wrapper for slides -->
                  <div class="carousel-inner" role="listbox">
                    <div class="item active">
                      <img src="image/birthday800x600.jpg" alt="Image">
                      <div class="carousel-caption">
                      </div>      
                    </div>

                    <div class="item">
                      <img src="image/colourblast800x600.jpg" alt="Image">
                      <div class="carousel-caption">
                      </div>      
                    </div>

                    <div class="item">
                      <img src="image/showreel800x600.jpg" alt="Image">
                      <div class="carousel-caption">
                      </div>      
                    </div>

                    <div class="item">
                      <img src="image/wallpaper_final.jpg" alt="Image">
                      <div class="carousel-caption">
                      </div>      
                    </div>

                    <div class="item">
                      <img src="image/wedo800x600.jpg" alt="Image">
                      <div class="carousel-caption">
                      </div>      
                    </div>

                  <!-- Left and right controls -->
                  <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                  </a>
                  <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                  </a>
                </div>
              </div>
                          <div class="row" style="margin-top: 15px;">
                <div class="col-sm-3">
                    <img src="image/childline_logo.gif" height="100%" width="100%" alt="Avatar">
                </div>

                <!-- first para -->
                <div class="col-sm-9">
                  <p     style="text-align: justify" ;>The work for the protection of the rights of all children in general. But our special focus is on all children in need of care and protection, especially the more vulnerable sections,</p>
                </div>
              </div>
              <h3 style=" padding-right: 15px; padding-left: 15px; margin-left: auto; text-align: center;">What We DO</h3>
              <p style=" padding-right: 15px; padding-left: 15px; margin-left: auto; text-align: justify"><b>Vision »</b><br>A child - friendly nation that guarantees the rights and protection of all children</p>
              <ul>
                <li>Street children and youth living alone on the streets.</li>
                <li>Child labourers working in the unorganised and organized sectors.</li>
                <li>Domestic help, especially girl domestics.</li>
                <li>Children affected by physical / sexual / emotional abuse in family, schools or institutions.</li>
                <li>Children who need emotional support and guidance.</li>
                <li>Children of commercial sex workers</li>
                <li>Child victims of the flesh trade</li>
                <li>Victims of child trafficking</li>
                <li>Children abandoned by parents or guardians</li>
                <li>Missing children.</li>
                <li>Run away children.</li>
                <li>Children who are victims of substance abuse</li>
                <li>Differently-abled children</li>
                <li>Children in conflict with the law</li>
                <li>Mentally challenged children</li>
                <li>HIV/ AIDS infected children</li>
              </ul> 
            </div>
        <!-- right sidebar -->
            <div class="col-sm-3 sidenav">
      <!-- Report Form  -->     
                <form method="post" action="ReportChildServlet" >
                		<input type="hidden" name="id" value='<%= userId %>'>
                    <h3>REPORT</h3>
                     <div class="well">
                        <label ><b>Name</b></label>
                          <br>
                        <input style="margin-bottom: 5px; width: 100%" type="text" placeholder="Your full name" name="name" required>
                    </div>  

                    <div class="well">
                        <label><b><strong>Address</strong></b></label>
                          <br>
                        <input style="margin-bottom: 5px; width: 100%" type="text" placeholder="Address Line 1" name="add1" required>       
                          <br>
                        <input style="margin-bottom: 5px; width: 100%" type="text" placeholder="Address Line 2" name="add2" required>       
                          <br>
                        <input style="margin-bottom: 5px; width: 100%" type="text" placeholder="CITY" name="city" required>       
                          <br>
                        <select style="margin-bottom: 5px; width: 100%" name="state" class="form-control selectpicker"  required>
                                <option value=" " >Please select your state</option>
                                <option value="Andaman and Nicobar Islands">Andaman and Nicobar Islands</option>
                                <option value="Andhra Pradesh">Andhra Pradesh</option>
                                <option value="Arunachal Pradesh">Arunachal Pradesh</option>
                                <option value="Assam">Assam</option>
                                <option value="Bihar">Bihar</option>
                                <option value="Chandigarh">Chandigarh</option>
                                <option value="Chhattisgarh">Chhattisgarh</option>
                                <option value="Dadra and Nagar Haveli">Dadra and Nagar Haveli</option>
                                <option value="Daman and Diu">Daman and Diu</option>
                                <option value="Delhi">Delhi</option>
                                <option value="Goa">Goa</option>
                                <option value="Gujarat">Gujarat</option>
                                <option value="Haryana">Haryana</option>
                                <option value="Himachal Pradesh">Himachal Pradesh</option>
                                <option value="Jammu and Kashmir">Jammu and Kashmir</option>
                                <option value="Jharkhand">Jharkhand</option>
                                <option value="Karnataka">Karnataka</option>
                                <option value="Kerala">Kerala</option>
                                <option value="Lakshadweep">Lakshadweep</option>
                                <option value="Madhya Pradesh">Madhya Pradesh</option>
                                <option value="Maharashtra">Maharashtra</option>
                                <option value="Manipur">Manipur</option>
                                <option value="Meghalaya">Meghalaya</option>
                                <option value="Mizoram">Mizoram</option>
                                <option value="Nagaland">Nagaland</option>
                                <option value="Orissa">Orissa</option>
                                <option value="Pondicherry">Pondicherry</option>
                                <option value="Punjab">Punjab</option>
                                <option value="Rajasthan">Rajasthan</option>
                                <option value="Sikkim">Sikkim</option>
                                <option value="Tamil Nadu">Tamil Nadu</option>
                                <option value="Tripura">Tripura</option>
                                <option value="Uttaranchal">Uttaranchal</option>
                                <option value="Uttar Pradesh">Uttar Pradesh</option>
                                <option value="West Bengal">West Bengal</option>
                        </select>
                        <input name="pin" placeholder="Pin Code" class="form-control"  type="number"  required> 
                        <input name="age" placeholder="Child Age If Known" class="form-control"  type="number" > 
                    </div>      

                    <div  class="well">                    
                        <label><b><strong>Discription</strong></b></label>
                            <br>
                        <textarea rows="3" style=" border: 1px solid #ccc; border-radius: 4px; box-shadow: inset 0 1px 1px rgba(0,0,0,.075); width: 100%" placeholder="Write your situation here"></textarea>
                            <br>
                    </div>
                    <div class="well"><!-- gender -->
                        <label class=""><b>Gender : </b></label>
                                <br>
                            <label>
                                <input type="radio" name="gender" value="Male"  required/> Male 
                            </label>
                            <br>
                            <label>
                                <input type="radio" name="gender" value="Female"  required/> Female
                            </label>
                    </div>   
                    
                    <div class="well"><!-- gender -->
                        <label class=""><b>Problem Type : </b></label>
                                <br>
                            <label>
                                <input type="radio" name="child" value="traf"  required/> Trafiking 
                            </label>
                            <br>
                            <label>
                                <input type="radio" name="child" value="labour"  required/> Labour
                            </label>
                            <label>
                                <input type="radio" name="child" value="abuse"  required/> Abuse or Protection
                            </label>
                    </div>                                   

                    <input type="submit"  class="btn btn-info btn-block" value="submit">
                </form>  
            </div>
          </div>
        </div>

        <footer class="container-fluid text-center">
          <p>Footer Text</p>
        </footer>

    </body>
</html>