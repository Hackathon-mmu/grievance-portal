<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<title>Donation Form</title>
	<meta charset="utf-8">
	  <meta name="viewport" content="width=device-width, initial-scale=1">
	  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	  <style>
	    /* Add a gray background color and some padding to the footer */
	    footer {
	      background-color: #f2f2f2;
	      padding: 25px;
	    }


	    .carousel-inner img {
	      width: 100%; /* Set width to 100% */
	      min-height: 200px;
	    }

	    /* Hide the carousel text when the screen is less than 600 pixels wide */
	    @media (max-width: 600px) {
	      .carousel-caption {
	        display: none;
	      }
	      input{
                border: 1px solid #ccc;
                border-radius: 4px;
                box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
            }
	    }
	  </style>
</head>
<body>
<%@ include file="WelcomeHeader.html" %>


	<div class="container">
	  <div style="align-items: center;" class="jumbotron">
	    <h1>donation Form</h1>
	    <form method="post" action="#" >
	    	<select style="margin-bottom: 5px; width: 75%" name="state" class="form-control selectpicker"  required="false">
                <option value=" " >select 75</option>
                <option value="NGO 1">NGO 1</option>
                <option value="NGO 2">NGO 2</option>
            </select><br>
            <b>₹ </b><input style="margin-bottom: 5px; width: 75%" type="number" placeholder="0000" name="ID" required>
            <div href="#" class="well" style="width: 75%">                    
                <label><b><strong>MESSAGE</strong></b></label>
                    <br>
                <textarea rows="3" style=" border: 1px solid #ccc; border-radius: 4px; box-shadow: inset 0 1px 1px rgba(0,0,0,.075);width : 50%" placeholder="Leave a message"></textarea>
                    <br>
             </div>
             <Input type="submit"  class="btn btn-info btn-block" value="submit">  

	  </div>
</div>

</body>
</html>