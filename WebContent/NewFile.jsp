<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Landing Page</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <style>
    /* Add a gray background color and some padding to the footer */
    footer {
      background-color: #f2f2f2;
      padding: 25px;
    }


    .carousel-inner img {
      width: 100%; /* Set width to 100% */
      min-height: 200px;
    }

    /* Hide the carousel text when the screen is less than 600 pixels wide */
    @media (max-width: 600px) {
      .carousel-caption {
        display: none;
      }
      
    }
  </style>
  
</head>
<body>
	<%@ include file="indexHeader.html" %>
	
	<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Login In</h4>
      </div>
      <div class="modal-body">
            <form method="post" action="LoginServlet">
        <h3>Please Login to continue</h3>
        <label><b>UserId</b></label>
          <br>
        <input type="text" placeholder="Enter Id" name="id" required>
        
          <br>
          <br>
        <label><b>Password</b></label>
          <br>
        <input type="Password" placeholder="Password" name="password" required>

          <br>
          <br>
     
      <input type="checkbox" checked="checked"> Remember me
        <br>
        <br>
      
        <input type="submit"  class="btn btn-info btn-block" value="Sign in!">
      </form>
      <br>
      <a	 href="ForgotPassword.html">Forgot your Password?</a>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
	
	<!-- Body Elements -->
	
	<div class="container">
<div class="row">
  <div class="col-sm-8">
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
      <!-- Indicators -->
      <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
        <li data-target="#myCarousel" data-slide-to="3"></li>
        <li data-target="#myCarousel" data-slide-to="4"></li>
        <li data-target="#myCarousel" data-slide-to="5"></li>
        <li data-target="#myCarousel" data-slide-to="6"></li>
      </ol>

      <!-- Wrapper for slides -->
      <div class="carousel-inner" role="listbox">
        <div class="item active">
          <img src="image/betiBachaoBetiPadhao.jpg" alt="Image">
          <div class="carousel-caption">
            <h3>Educate a girl child, Educate Society</h3>
            <p></p>
          </div>      
        </div>

        <div class="item">
          <img src="image/RTE.jpg" alt="Image">
          <div class="carousel-caption">
            <h3>Education for ALl</h3>
            <p>Eduaction is birth right!</p>
          </div>      
        </div>

        <div class="item">
          <img src="image/RTE_201301 2.jpg" alt="Image">
          <div class="carousel-caption">
              <h3>Education for ALl</h3>
              <p>Eduaction is birth right!</p>
          </div>      
        </div>

        <div class="item">
          <img src="image/Skill-Training.jpg" alt="Image">
          <div class="carousel-caption">
            <h3>Skill India</h3>
            <p>Learn and Earn!</p>
          </div>      
        </div>

        <div class="item">
          <img src="image/WomenEmpowerment 2.jpg" alt="Image">
          <div class="carousel-caption">
            <h2>Women Empowerment</h2>
            <p></p>
          </div>      
        </div>

        <div class="item">
          <img src="image/HUNGER.jpg" alt="Image">
          <div class="carousel-caption">
            <h4>SomeWhere in India, Someone is still starving</h4>
            <p>Donate food or money for this nobel cause!</p>
          </div>      
        </div>

        <div class="item">
          <img src="image/child-labor 2.jpg" alt="Image">
          <div class="carousel-caption">
            <h3>Rescue childrens from slavery!</h3>
            <p>Child labour, trafficking and child abuse is like slavery in 21st century.</p>
          </div>      
        </div>
      </div>

      <!-- Left and right controls -->
      <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div>
  </div>
   <div class="col-sm-4">
    <div class="well">
      <form method="post" action="LoginServlet">
        <h3>Please Login to continue</h3>
        <label><b>UserId</b></label>
          <br>
        <input type="text" placeholder="Enter Id" name="id" required>
        
          <br>
          <br>
        <label><b>Password</b></label>
          <br>
        <input type="Password" placeholder="Password" name="password" required>

          
          <br>
     
      <input type="checkbox" checked="checked"> Remember me
        <br>
        <br>
      
        <input type="submit"  class="btn btn-info btn-block" value="Sign in!">
      </form> 
      <br>
      <a	 href="ForgotPassword.html">Forgot your Password?</a>
    </div>
   

    <div class="well">
      <p>Not a Member yet? join hand in this greatness!</p>
      <button type="submit"  class="btn btn-success btn-block" >Sign Up</button>
    </div> 

  </div>
</div>
<hr>
</div>

  <div class="container text-center">    
    <h3>What We Do</h3>
      <br>
<!-- first child pannel -->
        <div class="panel panel-success">
          <div class="panel-heading">CHILD PROTECTION</div>
          <div class="panel-body">
            <div class="row">
              <div class="col-sm-3 col-md-4">
                <div class="thumbnail">
                  <img src="childline_logo.jpg" alt="">
                  <div class="caption">
                    <h3>Child Protection</h3>
                    <p><strong>CHILDLINE</strong>stands for a friendly <STRONG>'didi'</STRONG> or a sympathetic <strong>'bhaiya'</strong> who is always there for vulnerable children <strong>24 hours</strong> of the day, <strong>365 days</strong> of the year.</p>
                    <p><a href="#" class="btn btn-primary" role="button">Learn More</a>
                  </div>
                </div>
              </div>

              <div class="col-sm-3 col-md-4">
                <div class="thumbnail">
                  <img src="image/child-labour.jpg" alt="...">
                  <div class="caption">
                    <h3>Child Labour</h3>
                    <p>Child Labour is the practice of having children engage in economic activity. It is an illegal practise. <br>Help us to stop child labour by reporting</p>
                    <p><a href="#" class="btn btn-danger" role="button">REPORT</a></p>
                  </div>
                </div>
              </div>

            <div class="col-sm-3 col-md-4">
              <div class="thumbnail">
                <img src="image/child-trafficking.jpg" alt="...">
                <div class="caption">
                  <h3>CHILD TRAFICKING</h3>
                  <p>Child traficking is illegal in India.<br>but still, in India, there is a great need for convergence and implementation of comprehensive child protection mechanisms. Help us to implement this mechanism by reporting child traficking cases.</p>
                  <p><a href="#" class="btn btn-danger" role="button">Report</a></p>
                </div>
              </div>
            </div>
          </div>
        </div>
        </div><!-- first pannel ends here -->

<!-- second Women pannel begins here -->
        <div class="panel panel-info">
          <div class="panel-heading">WOMEN EMPOWERMENT</div>
          <div class="panel-body">
            <div class="row">
              <div class="col-sm-3 col-md-4">
                <div class="thumbnail">
                  <img src="http://placehold.it/120x120&text=image1" alt="">
                  <div class="caption">
                    <h3>Child Protection</h3>
                    <p><strong>CHILDLINE</strong>stands for a friendly <STRONG>'didi'</STRONG> or a sympathetic <strong>'bhaiya'</strong> who is always there for vulnerable children <strong>24 hours</strong> of the day, <strong>365 days</strong> of the year.</p>
                    <p><a href="#" class="btn btn-primary" role="button">Learn More</a>
                  </div>
                </div>
              </div>

              <div class="col-sm-3 col-md-4">
                <div class="thumbnail">
                  <img src="http://placehold.it/120x120&text=image2" alt="...">
                  <div class="caption">
                    <h3>Thumbnail label</h3>
                    <p>...</p>
                    <p><a href="#" class="btn btn-primary" role="button">Button</a></p>
                  </div>
                </div>
              </div>

            <div class="col-sm-3 col-md-4">
              <div class="thumbnail">
                <img src="http://placehold.it/120x120&text=image3" alt="...">
                <div class="caption">
                  <h3>Thumbnail label</h3>
                  <p>...</p>
                  <p><a href="#" class="btn btn-primary" role="button">Button</a></p>
                </div>
              </div>
            </div>
          </div>
        </div>
        </div><!-- Second pannel ends here -->

 <!-- Third Hunger pannel begins here -->
        <div class="panel panel-primary">
          <div class="panel-heading">HUNGER RESCUE</div>
          <div class="panel-body">
            <div class="row">
              <div class="col-sm-3 col-md-4">
                <div class="thumbnail">
                  <img src="childline_logo.gif" alt="">
                  <div class="caption">
                    <h3>Child Protection</h3>
                    <p><strong>CHILDLINE</strong>stands for a friendly <STRONG>'didi'</STRONG> or a sympathetic <strong>'bhaiya'</strong> who is always there for vulnerable children <strong>24 hours</strong> of the day, <strong>365 days</strong> of the year.</p>
                    <p><a href="#" class="btn btn-primary" role="button">Learn More</a>
                  </div>
                </div>
              </div>

              <div class="col-sm-3 col-md-4">
                <div class="thumbnail">
                  <img src="..." alt="...">
                  <div class="caption">
                    <h3>Thumbnail label</h3>
                    <p>...</p>
                    <p><a href="#" class="btn btn-primary" role="button">Button</a></p>
                  </div>
                </div>
              </div>

            <div class="col-sm-3 col-md-4">
              <div class="thumbnail">
                <img src="..." alt="...">
                <div class="caption">
                  <h3>Thumbnail label</h3>
                  <p>...</p>
                  <p><a href="#" class="btn btn-primary" role="button">Button</a></p>
                </div>
              </div>
            </div>
          </div>
        </div>
        </div><!-- third pannel ends here -->

<!-- Fourth unemployment pannel begins here -->
        <div class="panel panel-warning">
          <div class="panel-heading">UNEMPLOYMENT</div>
          <div class="panel-body">
            <div class="row">
              <div class="col-sm-3 col-md-4">
                <div class="thumbnail">
                  <img src="childline_logo.gif" alt="">
                  <div class="caption">
                    <h3>Child Protection</h3>
                    <p><strong>CHILDLINE</strong>stands for a friendly <STRONG>'didi'</STRONG> or a sympathetic <strong>'bhaiya'</strong> who is always there for vulnerable children <strong>24 hours</strong> of the day, <strong>365 days</strong> of the year.</p>
                    <p><a href="#" class="btn btn-primary" role="button">Learn More</a>
                  </div>
                </div>
              </div>

              <div class="col-sm-3 col-md-4">
                <div class="thumbnail">
                  <img src="..." alt="...">
                  <div class="caption">
                    <h3>Thumbnail label</h3>
                    <p>...</p>
                    <p><a href="#" class="btn btn-primary" role="button">Button</a></p>
                  </div>
                </div>
              </div>

            <div class="col-sm-3 col-md-4">
              <div class="thumbnail">
                <img src="..." alt="...">
                <div class="caption">
                  <h3>Thumbnail label</h3>
                  <p>...</p>
                  <p><a href="#" class="btn btn-primary" role="button">Button</a></p>
                </div>
              </div>
            </div>
          </div>
        </div>
        </div><!-- Fourth pannel ends here -->

<!-- Fifth Right and Dusties pannel begins here -->
        <div class="panel panel-danger"><!-- Fifth pannel begins here -->
          <div class="panel-heading">RIGHTS & DUTIES</div>
          <div class="panel-body">
            <div class="row">
              <div class="col-sm-3 col-md-4">
                <div class="thumbnail">
                  <img src="childline_logo.gif" alt="">
                  <div class="caption">
                    <h3>Child Protection</h3>
                    <p><strong>CHILDLINE</strong>stands for a friendly <STRONG>'didi'</STRONG> or a sympathetic <strong>'bhaiya'</strong> who is always there for vulnerable children <strong>24 hours</strong> of the day, <strong>365 days</strong> of the year.</p>
                    <p><a href="#" class="btn btn-primary" role="button">Learn More</a>
                  </div>
                </div>
              </div>

              <div class="col-sm-3 col-md-4">
                <div class="thumbnail">
                  <img src="..." alt="...">
                  <div class="caption">
                    <h3>Thumbnail label</h3>
                    <p>...</p>
                    <p><a href="#" class="btn btn-primary" role="button">Button</a></p>
                  </div>
                </div>
              </div>

            <div class="col-sm-3 col-md-4">
              <div class="thumbnail">
                <img src="..." alt="...">
                <div class="caption">
                  <h3>Thumbnail label</h3>
                  <p>...</p>
                  <p><a href="#" class="btn btn-primary" role="button">Button</a></p>
                </div>
              </div>
            </div>
          </div>
        </div>
        </div><!-- Fifth pannel ends here -->

        <!--<div class="panel panel-success">  Sixth pannel begins here
          <div class="panel-heading">Children Helpline</div>
          <div class="panel-body">
            <div class="row">
              <div class="col-sm-3 col-md-4">
                <div class="thumbnail">
                  <img src="childline_logo.gif" alt="">
                  <div class="caption">
                    <h3>Child Protection</h3>
                    <p><strong>CHILDLINE</strong>stands for a friendly <STRONG>'didi'</STRONG> or a sympathetic <strong>'bhaiya'</strong> who is always there for vulnerable children <strong>24 hours</strong> of the day, <strong>365 days</strong> of the year.</p>
                    <p><a href="#" class="btn btn-primary" role="button">Learn More</a>
                  </div>
                </div>
              </div>

              <div class="col-sm-3 col-md-4">
                <div class="thumbnail">
                  <img src="..." alt="...">
                  <div class="caption">
                    <h3>Thumbnail label</h3>
                    <p>...</p>
                    <p><a href="#" class="btn btn-primary" role="button">Button</a> <a href="#" class="btn btn-default" role="button">Button</a></p>
                  </div>
                </div>
              </div>

            <div class="col-sm-3 col-md-4">
              <div class="thumbnail">
                <img src="..." alt="...">
                <div class="caption">
                  <h3>Thumbnail label</h3>
                  <p>...</p>
                  <p><a href="#" class="btn btn-primary" role="button">Button</a> <a href="#" class="btn btn-default" role="button">Button</a></p>
                </div>
              </div>
            </div>
          </div>
        </div>
        </div> Sixth pannel ends here -->

    </div><!-- "what we do" ends here -->



<!-- all paneels end here -->
  </div>
  <hr>
</div>

<div class="container text-center">    
  <h3>Our Partners</h3>
  <br>
  <div class="row">
    <div class="col-sm-2">
      <img src="https://placehold.it/150x80?text=IMAGE" class="img-responsive" style="width:100%" alt="Image">
      <p>Partner 1</p>
    </div>
    <div class="col-sm-2"> 
      <img src="https://placehold.it/150x80?text=IMAGE" class="img-responsive" style="width:100%" alt="Image">
      <p>Partner 2</p>    
    </div>
    <div class="col-sm-2"> 
      <img src="https://placehold.it/150x80?text=IMAGE" class="img-responsive" style="width:100%" alt="Image">
      <p>Partner 3</p>
    </div>
    <div class="col-sm-2"> 
      <img src="https://placehold.it/150x80?text=IMAGE" class="img-responsive" style="width:100%" alt="Image">
      <p>Partner 4</p>
    </div> 
    <div class="col-sm-2"> 
      <img src="https://placehold.it/150x80?text=IMAGE" class="img-responsive" style="width:100%" alt="Image">
      <p>Partner 5</p>
    </div>     
    <div class="col-sm-2"> 
      <img src="https://placehold.it/150x80?text=IMAGE" class="img-responsive" style="width:100%" alt="Image">
      <p>Partner 6</p>
    </div> 
  </div>
</div><br>
	
	
	
	<!-- Body Elements Ends-->
	
	<%@ include file="indexFooter.html" %>
</body>
</html>