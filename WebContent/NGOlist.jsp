<%@page import="bean.NGOBean"%>
<%@page import="java.util.ArrayList"%>
<%@page import="dao.NgoImpl"%>
<%@page import="dao.Ngo"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
          <title>Template Sample</title>
          <meta charset="utf-8">
          <meta name="viewport" content="width=device-width, initial-scale=1">
          <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
          <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
          <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
          <style>
            /* Remove the navbar's default margin-bottom and rounded borders */ 
            .navbar {
              margin-bottom: 0;
              border-radius: 0;
              background-color: #3e4444;
            }

            input{
                border: 1px solid #ccc;
                border-radius: 4px;
                box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
            }
            
            /* Set height of the grid so .sidenav can be 100% (adjust as needed) */
            .row.content {height: 450px}
            
            /* Set gray background color and 100% height */
            .sidenav {
              padding-top: 20px;
              background-color: #f1f1f1;
              height: 665px;
              padding-bottom: 20px;
            }
            
            /* Set black background color, white text and some padding */
            footer {
              background-color: #555;
              color: white;
              padding: 15px;
            }
            
            /* On small screens, set height to 'auto' for sidenav and grid */
            @media screen and (max-width: 767px) {
              .sidenav {
                padding: 15px;
              }
              .row.content {height:auto;} 
            }
            .well{
                margin-bottom: 5px;
                padding: 10px;
            }
          </style>
    </head>
<body>
<%@ include file="WelcomeHeader.html" %>
<table class="table table-striped">
    <thead>
      <tr>
        <th>NGO Id</th>
        <th>Name</th>
        <th>Area</th>
        <th>NGO Type</th>
        <th>Email</th>
        <th>Fund Required</th>
        <th>Fund Received</th>
        <th>Fund Used</th>
        <th>Donate</th> 
      </tr>
    </thead>
    <tbody>
    
<%
	NgoImpl dao = new NgoImpl();
	ArrayList<NGOBean> li = dao.findAll();
	for(NGOBean bean: li) { %>
    
    
      <tr>
        <td><%=bean.getNgoId() %></td>
        <td><%=bean.getName()%></td>
        <td><%=bean.getArea()%></td>
        <td><%=bean.getType()  %></td>
        <td><%=bean.getEmail()  %></td>
        <td><%=bean.getTotalFund()  %></td>
        <td><%=bean.getFundRecieved()  %></td>
        <td><%=bean.getFundUsed()  %></td>
        <td><form meathod="get" action="Donation.jsp">
        	<input type="hidden" value='<%=bean.getNgoId()%>'>
        	<input type="submit" value='Donate'>
        </form></td>
      
<%  } %>
	
	</tbody>
  </table> 

</body>
</html>