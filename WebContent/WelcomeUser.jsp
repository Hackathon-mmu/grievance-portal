<%@page import="dao.UserImpl"%>
<%@page import="bean.UserBean"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
          <title>Template Sample</title>
          <meta charset="utf-8">
          <meta name="viewport" content="width=device-width, initial-scale=1">
          <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
          <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
          <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
          <style>
            /* Remove the navbar's default margin-bottom and rounded borders */ 
            .navbar {
              margin-bottom: 0;
              border-radius: 0;
              background-color: #3e4444;
            }

            input{
                border: 1px solid #ccc;
                border-radius: 4px;
                box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
            }
            
            /* Set height of the grid so .sidenav can be 100% (adjust as needed) */
            .row.content {height: 450px}
            
            /* Set gray background color and 100% height */
            .sidenav {
              padding-top: 20px;
              background-color: #f1f1f1;
              height: 665px;
              padding-bottom: 20px;
            }
            
            /* Set black background color, white text and some padding */
            footer {
              background-color: #555;
              color: white;
              padding: 15px;
            }
            
            /* On small screens, set height to 'auto' for sidenav and grid */
            @media screen and (max-width: 767px) {
              .sidenav {
                padding: 15px;
              }
              .row.content {height:auto;} 
            }
            .well{
                margin-bottom: 5px;
                padding: 10px;
            }
          </style>
    </head>

    <body>
    <%
	String userId = (String)session.getAttribute("userId");
	UserImpl dao = new UserImpl();
	UserBean bean = dao.findByID(userId);
	String name = bean.getName();
	
%>
    <!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Are You Sure You Want To LogOut?</h4>
      </div>
      <div class="modal-body">
            <form action="LogOutServlet" meathod="get">
            		<input type = hidden value ='<%= userId %>'>
            		<input type ="submit" value="LogOut">
            </form>
      <br>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

        <%@ include file="WelcomeHeader.html" %>

        <!-- left sidebar -->  
        <div class="container-fluid text-center">    
          <div class="row content">
            <div class="col-sm-3 well">
              <div class="well">
                <p><a href="#"></a><%= userId %></p>
                <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/d/d6/Bald_Eagle_Head_2_%286021915997%29.jpg/400px-Bald_Eagle_Head_2_%286021915997%29.jpg" class="img-circle" height="65" width="65" alt="Avatar">
              </div>

              <div class="well">
                <p><a href="#">Profile <%=name %></a></p>
                 <p>
                  <span class="label label-default"><%=name %></span>
                    <br>
                  <span class="label label-primary"></span>
                    <br>
                  <span class="label label-success"></span>
                    <br>
                  <%-- <span class="label label-info">Phone: <%=bean.getPhone() %></span>
                    <br>
                  <span class="label label-warning"><%=bean.getName() %></span>
                    <br>
                  <span class="label label-danger"><%=bean.getName() %></span>
                    <br> --%>
                </p> 
              </div>

              <div class="alert alert-success fade in">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                <p><strong>Ey!</strong></p>
                search nearby to know, how you can help your society!.
              </div>
              <div class="thumbnail">
                    <p>Upcoming Events:</p>
                    <img src="image/universumm.png" alt="Paris" width="400" height="300">
                    <p><strong>UniversuMM</strong> Star Night</p>
                    <p>Monday. 12 September 2017</p>
                    <button href="http://universumm.in/" class="btn btn-info">Learn More</button>
                </div>    
            </div>
        <!-- main central content -->
            <div class="col-sm-9 text-left container"> 
              <h1>Welcome <%=name %></h1>
              <div class="row">
                <div class="col-sm-3">
                    <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/d/d6/Bald_Eagle_Head_2_%286021915997%29.jpg/400px-Bald_Eagle_Head_2_%286021915997%29.jpg" height="90%" width="90%" alt="Avatar">
                </div>

                <!-- first para -->
                <div class="col-sm-9"></div>
                    <p     style="padding-right: 15px; padding-left: 15px; margin-left: auto; text-align: justify" ;>
                    		
                    </p>
              </div>
              <h3>Test</h3>
              <p>Lorem ipsum...</p>
            </div>
      
            </div>
          </div>
        </div>

        <footer class="container-fluid text-center">
          <p>Footer Text</p>
        </footer>

    </body>
</html>