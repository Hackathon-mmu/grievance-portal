package util;

import bean.CreditCardBean;

public interface Payment {
	boolean findByCardNumber(String userID, String cardNumber);
	String process(CreditCardBean bean, int amount);
}
