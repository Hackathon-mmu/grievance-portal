package servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bean.UserBean;
import dao.UserImpl;
import mails.Mailer;

/**
 * Servlet implementation class ForgotPassServlet
 */
public class ForgotPassServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String userId= request.getParameter("id");
		UserImpl dao = new UserImpl();
		UserBean bean = dao.findByID(userId);
		PrintWriter out = response.getWriter();
		if(bean!=null) {
			String email = bean.getEmail();
			String password = bean.getPassword();
			try {
				// from,password,to,subject,message
				Mailer mail = new Mailer();
				mail.send("javammu@gmail.com", "dellintel", email, "Password Reset", "Dear User, Please Note your Password:"+password);
				
				out.println("Email With Password Reset Sent");
				out.println("<a href='NewFile.jsp'>Go Back</a>");

			} catch (RuntimeException e) {
				System.out.println(e.getMessage());
				e.printStackTrace();
			}
			
		}
		else {
			out.println("Invalid UserId");
			
		}
	}

}
