package servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bean.ChildLabourBean;
import dao.ChildLabourImpl;

/**
 * Servlet implementation class ReportChildServlet
 */
public class ReportChildServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String userid = request.getParameter("id");
		String name = request.getParameter("name");
		String add = request.getParameter("add1")+" "+request.getParameter("add2")+" "+request.getParameter("city")+" "+request.getParameter("state");
		int pin = Integer.parseInt(request.getParameter("pin"));
		String gender = request.getParameter("gender");
		String problem = request.getParameter("child");
		int age = Integer.parseInt(request.getParameter("age"));
		PrintWriter out = response.getWriter();
		if(problem.equalsIgnoreCase("labour")) {
			ChildLabourBean bean =new ChildLabourBean();
			bean.setName(name);
			bean.setAddress(add);
			bean.setGender(gender);
			bean.setPin(pin);
			bean.setAge(age);
			ChildLabourImpl dao = new ChildLabourImpl();
			String result = dao.createChildLabour(bean);
			out.println(result);
			
			
		}
		else{
			out.println("under construction");
		}
		
	}

}
