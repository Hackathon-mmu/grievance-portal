package servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import bean.UserBean;
import dao.UserImpl;
import util.UtilUserImpl;

/**
 * Servlet implementation class SignUpServlet
 */
public class SignUpServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String firstName = request.getParameter("first_name");
		String lastName = request.getParameter("last_name");

		
		String dateOfBirth = request.getParameter("dob");

		String gender = request.getParameter("gender");
		String street = request.getParameter("address");
		String location = request.getParameter("location");
		String city = request.getParameter("city");
		String state = request.getParameter("state");
		String pincode = request.getParameter("pin");
		String mobileNo = request.getParameter("phone");
		String emailID = request.getParameter("email");
		String password = request.getParameter("password");
		String adhar = request.getParameter("adhar");
		int eduLevel = Integer.parseInt(request.getParameter("edu"));
		
		String [] ar = dateOfBirth.split("-");
		int year =Integer.parseInt( ar[2]);
		int age = 2017-year;
		
		System.out.println(firstName+lastName+dateOfBirth+gender+street+location+city+state+pincode+mobileNo+emailID+password);
		
		UserBean bean = new UserBean();
		bean.setName(firstName+" "+lastName);
		bean.setPassword(password);
		bean.setAge(age);
		bean.setAdhar(adhar);
		bean.setGender(gender);
		bean.setAddress(street+" "+location+" "+city+" "+state);
		bean.setPin(Integer.parseInt(pincode));
		bean.setPhone(mobileNo);
		bean.setEmail(emailID);
		bean.setUserType(2);
		
		
		UtilUserImpl user = new UtilUserImpl();
		String result = user.register(bean);
		
		PrintWriter out = response.getWriter();
		out.println(result);
		out.println("Please note your id and follow link in your email for activation");
	}

}
